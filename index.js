import express from "express";
import { v1 as uuidv1 } from "uuid";

const app = express();

app.use(express.json());

const USERS = [];

const VERIFY_CPF = (req, res, next) => {
  const { cpf } = req.params;
  const CPF_FOUND = USERS.find((user) => user.cpf === cpf);
  if (CPF_FOUND === undefined) {
    return res
      .status(404)
      .json({ error: "invalid cpf - user is not registered" });
  }
  next();
};

const VERIFY_NOTE_ID = (req, res, next) => {
  const { cpf, id } = req.params;
  const USER_FOUND = USERS.find((user) => user.cpf === cpf);
  const ID_NOTE = USER_FOUND.notes.find((noteId) => noteId.id === id);
  if (ID_NOTE === undefined) {
    return res.status(404).json({ error: "invalid id - id not registered" });
  }
  next();
};

app.post("/users", (req, res) => {
  const id = uuidv1();
  const { name, cpf } = req.body;
  const user = {
    id: id,
    name: name,
    cpf: cpf,
    notes: [],
  };

  USERS.push(user);
  res.status(201).json(user);
});

app.get("/users", (req, res) => {
  res.status(200).json(USERS);
});

app.patch("/users/:cpf(\\d+)", VERIFY_CPF, (req, res) => {
  const { cpf } = req.params;
  let { name } = req.body;
  const userIndex = USERS.findIndex((user) => user.cpf === cpf);

  if (req.body["cpf"] === undefined) {
    USERS[userIndex].name = name;
  } else if (name === undefined) {
    USERS[userIndex].cpf = req.body["cpf"];
  }

  res
    .status(200)
    .json({ message: "User is updated", users: [USERS[userIndex]] });
});

app.delete("/users/:cpf(\\d+)", VERIFY_CPF, (req, res) => {
  const { cpf } = req.params;
  const userIndex = USERS.findIndex((user) => user.cpf === cpf);
  USERS.splice(userIndex, 1);
  res.json({ message: "User is removed", users: [] });
});

app.post("/users/:cpf(\\d+)/notes", VERIFY_CPF, (req, res) => {
  const { cpf } = req.params;
  const id = uuidv1();
  const { title } = req.body;
  const { description } = req.body;
  const createdAt = new Date();

  const newNote = {
    id: id,
    title: title,
    description: description,
    createdAt: createdAt.toLocaleString(),
  };

  const user = USERS.find((user) => user.cpf === cpf);
  user.notes.push(newNote);

  res
    .status(201)
    .json({ message: `${title} was added into ${user.name}'s notes` });
});

app.get("/users/:cpf(\\d+)/notes", VERIFY_CPF, (req, res) => {
  const { cpf } = req.params;
  const user = USERS.find((user) => user.cpf === cpf);

  res.status(200).json(user.notes);
});

app.patch(
  "/users/:cpf(\\d+)/notes/:id",
  VERIFY_CPF,
  VERIFY_NOTE_ID,
  (req, res) => {
    const { cpf, id } = req.params;
    const { title, description } = req.body;
    const updatedAt = new Date();

    const user = USERS.find((user) => user.cpf === cpf);
    const note = user.notes.find((note) => note.id === id);

    note.updatedAt = updatedAt.toLocaleString();
    if (title === undefined) {
      note.description = description;
    } else if (description === undefined) {
      note.title = title;
    }

    res.status(200).json(note);
  }
);

app.delete(
  "/users/:cpf(\\d+)/notes/:id",
  VERIFY_CPF,
  VERIFY_NOTE_ID,
  (req, res) => {
    const { cpf, id } = req.params;
    const user = USERS.find((user) => user.cpf === cpf);
    const noteIndex = user.notes.findIndex((note) => note.id === id);

    user.notes.splice(noteIndex, 1);
    res.json({ message: "Note is removed", notes: [] });
  }
);

app.listen(3000, () => {});
